// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BeginnerCourse/BeginnerCourseGameModeBase.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBeginnerCourseGameModeBase() {}
// Cross Module References
	BEGINNERCOURSE_API UClass* Z_Construct_UClass_ABeginnerCourseGameModeBase();
	BEGINNERCOURSE_API UClass* Z_Construct_UClass_ABeginnerCourseGameModeBase_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_BeginnerCourse();
// End Cross Module References
	void ABeginnerCourseGameModeBase::StaticRegisterNativesABeginnerCourseGameModeBase()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ABeginnerCourseGameModeBase);
	UClass* Z_Construct_UClass_ABeginnerCourseGameModeBase_NoRegister()
	{
		return ABeginnerCourseGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ABeginnerCourseGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABeginnerCourseGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_BeginnerCourse,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABeginnerCourseGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "BeginnerCourseGameModeBase.h" },
		{ "ModuleRelativePath", "BeginnerCourseGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABeginnerCourseGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABeginnerCourseGameModeBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ABeginnerCourseGameModeBase_Statics::ClassParams = {
		&ABeginnerCourseGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ABeginnerCourseGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABeginnerCourseGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABeginnerCourseGameModeBase()
	{
		if (!Z_Registration_Info_UClass_ABeginnerCourseGameModeBase.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ABeginnerCourseGameModeBase.OuterSingleton, Z_Construct_UClass_ABeginnerCourseGameModeBase_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ABeginnerCourseGameModeBase.OuterSingleton;
	}
	template<> BEGINNERCOURSE_API UClass* StaticClass<ABeginnerCourseGameModeBase>()
	{
		return ABeginnerCourseGameModeBase::StaticClass();
	}
	ABeginnerCourseGameModeBase::ABeginnerCourseGameModeBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABeginnerCourseGameModeBase);
	ABeginnerCourseGameModeBase::~ABeginnerCourseGameModeBase() {}
	struct Z_CompiledInDeferFile_FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ABeginnerCourseGameModeBase, ABeginnerCourseGameModeBase::StaticClass, TEXT("ABeginnerCourseGameModeBase"), &Z_Registration_Info_UClass_ABeginnerCourseGameModeBase, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ABeginnerCourseGameModeBase), 3968147762U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_1919415934(TEXT("/Script/BeginnerCourse"),
		Z_CompiledInDeferFile_FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
