// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "BeginnerCourseGameModeBase.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BEGINNERCOURSE_BeginnerCourseGameModeBase_generated_h
#error "BeginnerCourseGameModeBase.generated.h already included, missing '#pragma once' in BeginnerCourseGameModeBase.h"
#endif
#define BEGINNERCOURSE_BeginnerCourseGameModeBase_generated_h

#define FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_SPARSE_DATA
#define FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_RPC_WRAPPERS
#define FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_ACCESSORS
#define FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABeginnerCourseGameModeBase(); \
	friend struct Z_Construct_UClass_ABeginnerCourseGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ABeginnerCourseGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/BeginnerCourse"), NO_API) \
	DECLARE_SERIALIZER(ABeginnerCourseGameModeBase)


#define FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesABeginnerCourseGameModeBase(); \
	friend struct Z_Construct_UClass_ABeginnerCourseGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ABeginnerCourseGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/BeginnerCourse"), NO_API) \
	DECLARE_SERIALIZER(ABeginnerCourseGameModeBase)


#define FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABeginnerCourseGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABeginnerCourseGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABeginnerCourseGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABeginnerCourseGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABeginnerCourseGameModeBase(ABeginnerCourseGameModeBase&&); \
	NO_API ABeginnerCourseGameModeBase(const ABeginnerCourseGameModeBase&); \
public: \
	NO_API virtual ~ABeginnerCourseGameModeBase();


#define FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABeginnerCourseGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABeginnerCourseGameModeBase(ABeginnerCourseGameModeBase&&); \
	NO_API ABeginnerCourseGameModeBase(const ABeginnerCourseGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABeginnerCourseGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABeginnerCourseGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABeginnerCourseGameModeBase) \
	NO_API virtual ~ABeginnerCourseGameModeBase();


#define FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_12_PROLOG
#define FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_SPARSE_DATA \
	FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_RPC_WRAPPERS \
	FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_ACCESSORS \
	FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_INCLASS \
	FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_SPARSE_DATA \
	FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_ACCESSORS \
	FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BEGINNERCOURSE_API UClass* StaticClass<class ABeginnerCourseGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Development_UnrealProj_BeginnerCourse_Source_BeginnerCourse_BeginnerCourseGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
